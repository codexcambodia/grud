<div class="card mb-3">
    <div class="card-header">
        {{ __('crud.'.$table.'.'.$virtualFieldName)  }}
        <input type="hidden" name="{{ $virtualFieldName }}" value="">
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table id="dataTable" class="table table-bordered table-hover display" style="width:100%">
                <thead>
                    <tr>
                      <th></th>
                      <th>{{ __('crud.'.$table.'.'.'id')  }}</th>
                      @foreach($list as $column)
                          <th>{{ __('crud.'.$table.'.'.$column->field)  }}</th>
                      @endforeach
                      
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    
</div>

     

<script>
  var {{$virtualFieldName}}_selectedIDs = {!! json_encode($selectedIDs) !!};
  
  function {{$virtualFieldName}}_bindSelectIDs(){
    var result = '';
    var sep = '';
    for(var i =0; i < {{$virtualFieldName}}_selectedIDs.length; i++){
        result = result + sep + {{$virtualFieldName}}_selectedIDs[i];
        sep = ',';
    }
    $('[name={{$virtualFieldName}}]').val(result);
  }

  function {{$virtualFieldName}}_updateSelectIDs(id,obj){
    var isChecked = $(obj).prop('checked');
    
    if(isChecked){
        if(!{{$virtualFieldName}}_selectedIDs.includes(id)){
            {{$virtualFieldName}}_selectedIDs.push(id);
            {{$virtualFieldName}}_bindSelectIDs();
            return;
        }
    }
    for( var i = 0; i < {{$virtualFieldName}}_selectedIDs.length; i++){ 
                                   
        if ( {{$virtualFieldName}}_selectedIDs[i] === id) { 
            {{$virtualFieldName}}_selectedIDs.splice(i, 1); 
            i--; 
        }
    }
    {{$virtualFieldName}}_bindSelectIDs();

        
  
    
    
  }
  var convertedSingleSelectSources = {!! json_encode($convertedSingleSelectSources) !!};
  var transformedDictFields = {!! json_encode($transformedDictFields) !!};
  $(document).on('ready', function() {

        {{$virtualFieldName}}_bindSelectIDs();

        var table = $('#dataTable').DataTable({
        processing: true,
        serverSide: true,
        order: [[ 1, 'asc' ]],
        ajax: {
            url: '{{ route($route.'.index-data')}}',
            data: function(d) {
                // $('div.dataTables_length select').addClass('form-control').addClass('input-sm');
                // $('div#data-table_filter input').addClass('form-control').addClass('input-sm');
                // //   d.name = $('input[name=name]').val();
                //   d.email = $('input[name=email]').val();
            }
        },
        columnDefs: [
            {
                targets: 0,
                className: 'text-center',
                width: '50px'
            },
            {
                targets: -1,
                className: 'text-center',
                width: '120px'
            }
        ],
        columns: [
            {!! "{ data: 'id', name: 'id', render: function ( data, type, row, meta ) { 
                console.log(data);
                if(".$virtualFieldName."_selectedIDs.includes(data)){
                    return '<input onchange=\"".$virtualFieldName."_updateSelectIDs('+data+',this)\" class=\"check_id\" type=\"checkbox\" CHECKED/>';
                 
                }
                return '<input onchange=\"".$virtualFieldName."_updateSelectIDs('+data+',this)\"  class=\"check_id\" type=\"checkbox\"/>';
                
            } }," !!}

            {!! "{ data: 'id', name: 'id' }," !!}
            
            @foreach($list as $column)
              @if(in_array($column->field,$dateFields))
                  {!! "{ data: '".$column->field."', name: '".$column->field."', render: function ( data, type, row, meta ) { 
                      return moment(new Date(data)).format('DD/MM/YYYY');

                  } }," !!}
              @elseif(array_key_exists($column->field,$convertedSingleSelectSources))
                  {!! "{ data: '".$column->field."', name: '".$column->field."', render: function ( data, type, row, meta ) { 
                      return convertedSingleSelectSources.".$column->field."[data];

                  } }," !!}
              @elseif(array_key_exists($column->field,$transformedDictFields))
                  {!! "{ data: '".$column->field."', name: '".$column->field."', render: function ( data, type, row, meta ) { 
                      if(data != null){
                        return transformedDictFields.".$column->field."[data];
                      }
                      return data;
                      

                  } }," !!}
              @elseif(array_key_exists($column->field,$imageFields))
                @php
                    $imageField = $imageFields[$column->field];
                    $width = $imageField[0] / 2;
                    $height = $imageField[1] / 2;
                @endphp
                  {!! "{ data: '".$column->field."', name: '".$column->field."', render: function ( data, type, row, meta ) { 
                      return '<img style=\"width : ".$width."px; hegiht: ".$height."px;\" class=\"img-thumbnail\" src=\"".url("/media/small")."/'+data+'\" />';
                      
                  } }," !!}
                @else
                    {!! "{ data: '".$column->field."', name: '".$column->field."' }," !!}
                @endif
            @endforeach
                

        ]
    });

});
</script>

