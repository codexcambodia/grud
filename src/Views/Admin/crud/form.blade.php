@extends('layouts.admin')

@section('breadcrumb-holder')

  <h1 class="main-title float-left">{{ __('crud.'.$table)}}</h1>
  <ol class="breadcrumb float-right">
      <li class="breadcrumb-item">Home</li>
      <li class="breadcrumb-item active">{{ __('crud.'.$table)}}</li>
  </ol>
@stop

@section('content')
<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
  <div class="card mb-3">
      <div class="card-header">
          <h3><i class="far fa-check-square"></i> {{ isset($item) ? __('crud.'.$table.'.edit') :  __('crud.'.$table.'.create')  }}</h3>
          
      </div>

      <div class="card-body">

          {{ Form::open(array('url' => isset($item) ? route($route.'.update', [$item->id]) : route($route.".store"), 'method' => isset($item) ? 'PUT' : 'POST', 'class'=>'col-md-12', 'role' => 'form')) }}
          @csrf

          {!! Form::input('hidden','id', $item->id ?? '') !!}
              @foreach($form->rows[0] as $group)
              @php
                  $field = $group->field;
                  $value = $item->$field ?? '';  
                  
              @endphp
              @if(array_key_exists($field,$singleSelectSources))
                  <div class="form-group">
                    <label for="{{ $group->field }}">{{ __('crud.'.$table.'.'.$group->field)  }}</label>
                    {{ Form::select($group->field, $singleSelectSources[$field], $value, ['class' => 'form-control select2']) }}
                    @error($field)
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                  </div>
              @elseif(array_key_exists($field,$transformedDictFields))
                  <div class="form-group">
                    <label for="{{ $group->field }}">{{ __('crud.'.$table.'.'.$group->field)  }}</label>
                    {{ Form::select($group->field, $transformedDictFields[$field], $value, ['class' => 'form-control select2']) }}
                    @error($field)
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                  </div>
              @elseif(in_array($field,$textAreaFields))
                  <div class="form-group">
                        <label for="{{ $group->field }}">{{ __('crud.'.$table.'.'.$group->field)  }}</label>
                        {{ Form::input('textarea', $group->field, $value, ['class' => 'form-control', 'id' => $group->field, 'placeholder' => __('crud.'.$table.'.'.$group->field)]) }}
                        @error($field)
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                  </div>
              @elseif(in_array($field,$dateFields))
              
                  <div class="form-group">
                        @php
                            if($value != ''){
                                $value = $value->format('d/m/Y');
                            }
                        @endphp
                        <label for="{{ $group->field }}">{{ __('crud.'.$table.'.'.$group->field)  }}</label>
                        {{ Form::input('text', $group->field, $value, ['class' => 'form-control singledatepicker', 'id' => $group->field, 'placeholder' => __('crud.'.$table.'.'.$group->field), in_array($field, $requiredFields) ? 'required' : '']) }}
                        @error($field)
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                  </div>
              @elseif(in_array($field,$dateFields))
              
                  <div class="form-group">
                      @php
                          if($value != ''){
                            $value = $value->format('d/m/Y');
                          }
                      @endphp
                      <label for="{{ $group->field }}">{{ __('crud.'.$table.'.'.$group->field)  }}</label>
                      <input type="text" name="{{ $group->field }}" value="{{ $value }}" class="form-control singledatepicker" id="{{ $group->field }}" placeholder="{{ __('crud.'.$table.'.'.$group->field)  }}">
                        @error($field)
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                  </div>
              @elseif(in_array($field, $passwordFields) || in_array($field, $passwordConfirmationFields))
                    <div class="form-group">
                        <label for="{{ $group->field }}">{{ __('crud.'.$table.'.'.$group->field)  }}</label>
                        <input type="password" name="{{ $group->field }}" value="" class="form-control" id="{{ $group->field }}" placeholder="{{ __('crud.'.$table.'.'.$group->field)  }}" {{ in_array($field,$requiredFields) && !isset($item) ? 'required' : '' }}>
                        @error($field)
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
              @elseif(array_key_exists($field,$imageFields))

                  <div class="form-group">
                    @php
                        $imageField = $imageFields[$field];
                        $width = $imageField[0];
                        $height = $imageField[1];
                        $logo_url = 'https://via.placeholder.com/'.$width.'x'.$height;
                        if($value != '' ) {
                            $logo_url = url('media/large/'.$value);
                        }
                    @endphp
                    <label for="{{ $group->field }}">{{ __('crud.'.$table.'.'.$group->field)  }}</label>
                    {{-- <div class="form-control"> --}}
                    <img onclick="uploadImageFor('{{ $group->field }}')" data-name="{{ $group->field }}" class="form-control" src="{{ $logo_url }}" style="width: {{$width}}px; height: {{$height}}px;">
                      
                    {{-- </div> --}}
                    <input type="hidden" name="{{ $group->field }}" value="{{ $value }}" id="{{ $group->field }}" placeholder="{{ __('crud.'.$table.'.'.$group->field)  }}">
                    @error($field)
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                  </div>
                  <div class="clearfix"></div>
              @else
                  <div class="form-group">
                      <label for="{{ $group->field }}">{{ __('crud.'.$table.'.'.$group->field)  }}</label>
                      {{ Form::input('text', $group->field, $value, ['class' => 'form-control', 'id' => $group->field, 'placeholder' => __('crud.'.$table.'.'.$group->field), in_array($field, $requiredFields) ? 'required' : '']) }}
                        @error($field)
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                  </div>
              @endif
              
              @endforeach

              @foreach($extendedFormViews as $view)
                @include($view)
              @endforeach
              
              <button type="button" onclick="cancel()" class="btn btn-secondary">{{__('crud.cancel')}}</button>
              <button type="submit" class="btn btn-primary">{{__('crud.save')}}</button>
          {{ Form::close() }}

      </div>
  </div><!-- end card-->
</div>

<form method="POST" id="uploadFileForm" action="{{route('upload.image')}}" accept-charset="UTF-8" id="uploadFormProfile" enctype="multipart/form-data">
  @csrf
  <input id="uploadFile" style="display:none;" accept="image/png, image/jpeg, image/svg+xml" class="uploadFileProfile" name="file" type="file">
</form>

@stop
@section('scripts')
<script>
  function cancel(){
    document.location = '{{route($route.'.index')}}';
  }
  $(document).on('ready',function() {
      $('.select2').select2();
      $('.singledatepicker').daterangepicker({
          singleDatePicker: true,
          showDropdowns: true,
          locale: {
            format: 'DD/MM/YYYY'
          }
      });
  });

  var elementNameUploadImageFor = null;
  function uploadImageFor(elementName){
    elementNameUploadImageFor = elementName;
    $('#uploadFile').click();
  }

  $(document).ready(function(){
    $('#uploadFile').change(function() {
        $( "#uploadFileForm" ).submit();
    });
  });

  $('#uploadFileForm').on('submit',(function(e) {
        e.preventDefault();
        var _token = $('input[name="_token"]').val();
        var formData = new FormData();
        formData.append('file', $('#uploadFile')[0].files[0]);
        formData.append('_token', _token)
        $.ajax({
            type:'POST',
            url: $(this).attr('action'),
            data:formData,
            cache:false,
            contentType: false,
            processData: false,
            beforeSend: function() {
                // $('.uploadProfile').html('<img src="' + window.location.origin + '/images/ajax-loading-64.gif" class="img-responsive" alt="">' + fa_camera_icon);
            },
            success:function(data) {
                $('[data-name='+elementNameUploadImageFor+']').attr('src','{{url('/')}}'+'/uploaded/images/'+data.file_name);
                $('[name='+elementNameUploadImageFor+']').val(data.file_name);
            },
            error:function(data){
                
                
            }
        });
    }));
</script>
@stop