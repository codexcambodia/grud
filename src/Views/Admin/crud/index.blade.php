@extends('layouts.admin')

@section('breadcrumb-holder')

  <ol class="breadcrumb float-left">
      <li class="breadcrumb-item">Home</li>
      <li class="breadcrumb-item active">{{ __('crud.'.$table)}}</li>
  </ol>
@stop

@section('content')
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
  <div class="card mb-3">
      <div class="card-header">
        <div class="row">
          <h3 class="col-md-6"><i class="fas fa-table"></i> {{ __('crud.'.$table.'.list')  }}&nbsp;&nbsp;&nbsp;</h3>
          <div class="col-md-6 button-list text-right pr-1">
            <a role="button" href="{{ route($route.'.create') }}" class="btn btn-primary btn-sm">
                <i class="fas fa-plus"></i>
            </a>
          </div>
        </div>
      </div>

      <div class="card-body">
          <div class="table-responsive">
              <table id="dataTable" class="table table-bordered table-hover display" style="width:100%">
                  <thead>
                      <tr>
                        <th></th>
                        @foreach($list as $column)
                            <th>{{ __('crud.'.$table.'.'.$column->field)  }}</th>
                        @endforeach
                        <th>{{ __('Action') }}</th>
                      </tr>
                  </thead>
              </table>
          </div>
          <!-- end table-responsive-->

      </div>
      <!-- end card-body-->

  </div>
  <!-- end card-->

</div>

<div class="modal fade custom-modal" id="m-sm" tabindex="-1" role="dialog" aria-labelledby="customModal" aria-hidden="true">
  <div class="modal-dialog" role="document">
      <div class="modal-content">
          <div class="modal-header">
              <h5 class="modal-title"></h5>
          </div>
          <div class="modal-body">
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-secondary modal-no" data-dismiss="modal">No</button>
              <button type="button" class="btn btn-danger modal-yes">Yes</button>
          </div>
      </div>
  </div>
</div>
{{ Form::open(["url" => '', "method" => "DELETE", "enctype" => "multipart/form-data", "id" => "deleteForm", "class" => "hidden"]) }}
{{ Form::close() }}
<script>
function openConfirmDelete(id){
  openModalConfirm('{{ __("Are you sure?")}}','{{ __("You are about to delete.")}}',function(){
    $('#deleteForm').attr('action','{{ route($route.'.index') }}/'+id);
    $('#deleteForm').submit();
  }, function(){
    
  })
}
function openModalConfirm(title,body,actionYes,actionNo){
  $('#m-sm').find('.modal-title').html(title);
  $('#m-sm').find('.modal-body').html(body);
  $('#m-sm').find('.modal-yes').click(actionYes);
  $('#m-sm').find('.modal-no').click(actionNo);
  $('#m-sm').modal('show');

}
</script>

<!-- ############ PAGE END-->   
@stop

@section('scripts')
<script>
  var convertedSingleSelectSources = {!! json_encode($convertedSingleSelectSources) !!};
  var transformedDictFields = {!! json_encode($transformedDictFields) !!};
  $(document).on('ready', function() {

        var table = $('#dataTable').DataTable({
        processing: true,
        serverSide: true,
        order: [[ 1, 'asc' ]],
        ajax: {
            url: '{{ route($route.'.index-data')}}',
            data: function(d) {
                // $('div.dataTables_length select').addClass('form-control').addClass('input-sm');
                // $('div#data-table_filter input').addClass('form-control').addClass('input-sm');
                // //   d.name = $('input[name=name]').val();
                //   d.email = $('input[name=email]').val();
            }
        },
        columnDefs: [
            {
                targets: 0,
                className: 'text-center',
                width: '50px'
            },
            {
                targets: -1,
                className: 'text-center',
                width: '120px'
            }
        ],
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},

            @foreach($list as $column)
              @if(in_array($column->field,$dateFields))
                  {!! "{ data: '".$column->field."', name: '".$column->field."', render: function ( data, type, row, meta ) { 
                      return moment(new Date(data)).format('DD/MM/YYYY');

                  } }," !!}
              @elseif(array_key_exists($column->field,$convertedSingleSelectSources))
                  {!! "{ data: '".$column->field."', name: '".$column->field."', render: function ( data, type, row, meta ) { 
                      return convertedSingleSelectSources.".$column->field."[data];

                  } }," !!}
              @elseif(array_key_exists($column->field,$transformedDictFields))
                  {!! "{ data: '".$column->field."', name: '".$column->field."', render: function ( data, type, row, meta ) { 
                      if(data != null){
                        var obj = transformedDictFields.".$column->field.";
                        if(data in obj){
                          return obj[data];
                        }
                        
                      }
                      return data;
                      

                  } }," !!}
              @elseif(array_key_exists($column->field,$imageFields))
                @php
                    $imageField = $imageFields[$column->field];
                    $width = $imageField[0] / 2;
                    $height = $imageField[1] / 2;
                @endphp
                  {!! "{ data: '".$column->field."', name: '".$column->field."', render: function ( data, type, row, meta ) { 
                      return '<img style=\"width : ".$width."px; hegiht: ".$height."px;\" class=\"img-thumbnail\" src=\"".url("/media/small")."/'+data+'\" />';
                      
                  } }," !!}
                @else
                    {!! "{ data: '".$column->field."', name: '".$column->field."' }," !!}
                @endif
            @endforeach
                {    
                    data: 'id',
                    render: function ( data, type, row, meta ) { 
                        return '<a href="{{ route($route.'.index') }}/'+data+'/edit" class="btn btn-sm fs-10 btn-primary"><i class="fa fa-edit"></i> Edit</a> <a href="#" onclick="openConfirmDelete('+data+')" class="btn btn-sm fs-10 btn-danger"><i class="fa fa-trash"></i> Delete</a>' 
                    }, orderable: false, searchable: false
                }

        ]
    });

});
</script>
@stop
