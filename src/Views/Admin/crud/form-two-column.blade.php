@extends('layouts.admin')

@section('breadcrumb-holder')

  <ol class="breadcrumb float-left">
      <li class="breadcrumb-item">Home</li>
      <li class="breadcrumb-item active">{{ __('crud.'.$table)}}</li>
  </ol>
@stop

@section('content')
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
  <div class="card mb-3">
      <div class="card-header">
          <h3><i class="far fa-check-square"></i> {{ isset($item) ? __('crud.'.$table.'.edit') :  __('crud.'.$table.'.create')  }}</h3>
          
      </div>

      <div class="card-body">

          {{ Form::open(array('url' => isset($item) ? route($route.'.update', [$item->id]) : route($route.".store"), 'method' => isset($item) ? 'PUT' : 'POST', 'class'=>'col-md-12', 'role' => 'form')) }}
          @csrf

          {!! Form::input('hidden','id', $item->id ?? '') !!}
              @foreach($form->rows as $row)
              <div class="form-row">
                @foreach($row as $group)
                  @php
                      $field = $group->field;
                      $value = $item->$field ?? '';  
                      
                  @endphp
                  @if(array_key_exists($field,$singleSelectSources))
                      <div class="form-group col-md-6">
                        <label for="{{ $group->field }}">{{ __('crud.'.$table.'.'.$group->field)  }}</label>
                        {{-- <select class="form-control select2" name="{{ $group->field }}" data-select2-id="example1" tabindex="-1" aria-hidden="true">
                            @foreach($singleSelectSources[$field] as $record)
                                <option value="{{$record->id}}" {{$record->id == $value ? 'SELECTED' : ''}}>{{$record->display}}</option>
                            @endforeach
                        </select> --}}
                        {{ Form::select($group->field, $singleSelectSources[$field], $value, ['class' => 'form-control select2']) }}
                        @error($field)
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                      </div>
                    @elseif(array_key_exists($field,$transformedDictFields))
                      <div class="form-group col-md-6">
                        <label for="{{ $group->field }}">{{ __('crud.'.$table.'.'.$group->field)  }}</label>
                        {{ Form::select($group->field, $transformedDictFields[$field], $value, ['class' => 'form-control select2']) }}
                        @error($field)
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                      </div>
                  @elseif(in_array($field,$textAreaFields))
                      <div class="form-group col-md-6">
                          <label for="{{ $group->field }}">{{ __('crud.'.$table.'.'.$group->field)  }}</label>
                            {{ Form::textarea($group->field, $value, ['class' => 'form-control', 'id' => $group->field, 'placeholder' => __('crud.'.$table.'.'.$group->field), 'rows' => 8]) }}
                            @error($field)
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                      </div>
                  @elseif(in_array($field,$dateFields))
                  
                      <div class="form-group col-md-6">
                          @php
                          
                              if($value != ''){
                                // dd($value);
                                $value = $value->format('d/m/Y');
                              }
                          @endphp
                          <label for="{{ $group->field }}">{{ __('crud.'.$table.'.'.$group->field)  }}</label>
                            {{ Form::input('text', $group->field, $value, ['class' => 'form-control singledatepicker', 'id' => $group->field, 'placeholder' => __('crud.'.$table.'.'.$group->field), in_array($field, $requiredFields) ? 'required' : '']) }}
                            @error($field)
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                      </div>
                  @elseif(in_array($field, $passwordFields) || in_array($field, $passwordConfirmationFields))
                        <div class="form-group col-md-6">
                            <label for="{{ $group->field }}">{{ __('crud.'.$table.'.'.$group->field)  }}</label>
                            <input type="password" name="{{ $group->field }}" value="" class="form-control" id="{{ $group->field }}" placeholder="{{ __('crud.'.$table.'.'.$group->field)  }}" {{ in_array($field, $requiredFields) && !isset($item) ? 'required' : '' }}>
                            @error($field)
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                  @elseif(array_key_exists($field,$imageFields))

                      <div class="form-group pl-1">
                        @php
                            $imageField = $imageFields[$field];
                            $width = $imageField[0];
                            $height = $imageField[1];
                            $logo_url = 'https://via.placeholder.com/'.$width.'x'.$height;
                            if($value != '' ) {
                                $logo_url = url('media/large/'.$value);
                            }
                        @endphp
                        <label for="{{ $group->field }}">{{ __('crud.'.$table.'.'.$group->field)  }}</label>
                        {{-- <div class="form-control"> --}}
                        <!-- https://via.placeholder.com/{{$width}}x{{$height}} -->
                        <img onclick="uploadImageFor('{{ $group->field }}')" data-name="{{ $group->field }}" class="form-control p-0 cursor-pointer" src="{{ $logo_url }}" style="width: {{$width}}px; height: {{$height}}px;">
                          
                        {{-- </div> --}}
                        <input type="hidden" name="{{ $group->field }}" value="{{ $value }}" id="{{ $group->field }}" placeholder="{{ __('crud.'.$table.'.'.$group->field)  }}">
                        @error($field)
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                      </div>
                      <div class="clearfix"></div>
                  @elseif(in_array($field, $fileFields))
                      <div class="form-group col-md-12">
                        <a onclick="uploadFileFor('{{ $group->field }}')" href="javascript:void(0);" class="btn btn-primary">{{ __('crud.'.$table.'.'.$group->field)  }}</a>
                        <input type="hidden" name="{{ $group->field }}" value="{{ $value }}" id="{{ $group->field }}" placeholder="{{ __('crud.'.$table.'.'.$group->field)  }}">
                        @error($field)
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                      </div>
                      <div class="clearfix"></div>
                  @else
                      <div class="form-group col-md-6">
                          <label for="{{ $group->field }}">{{ __('crud.'.$table.'.'.$group->field)  }}</label>
                          {{ Form::input('text', $group->field, $value, ['class' => 'form-control', 'id' => $group->field, 'placeholder' => __('crud.'.$table.'.'.$group->field), in_array($field, $requiredFields) ? 'required' : '']) }}
                            @error($field)
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                      </div>
                  @endif
                @endforeach
              </div>
              @endforeach
                
                @error('virtual_companies')
                    <div class="form-group">
                        <span class="text-danger">{{ $message }}</span>
                    </div>
                @enderror

                @error('virtual_products')
                    <div class="form-group">
                        <span class="text-danger">{{ $message }}</span>
                    </div>
                @enderror
                
                @if(isset($releatedCrudMultipleViews))
                    @foreach($releatedCrudMultipleViews as $releatedCrudMultipleView)
                        {!! $releatedCrudMultipleView !!}
                    @endforeach
                @endif

                @foreach($extendedFormViews as $view)
                    @include($view)
                @endforeach
                
                <button type="button" onclick="cancel()" class="btn btn-secondary">{{__('crud.cancel')}}</button>
                <button type="submit" class="btn btn-primary">{{__('crud.save')}}</button>
             
              {{ Form::close() }}

      </div>
  </div><!-- end card-->
</div>

<form method="POST" id="uploadImageForm" action="{{route('upload.image')}}" accept-charset="UTF-8" id="uploadFormProfile" enctype="multipart/form-data">
  @csrf
  <input id="uploadImage" style="display:none;" accept="image/png, image/jpeg, image/svg+xml" class="uploadFileProfile" name="file" type="file">
</form>

<form method="POST" id="uploadFileForm" action="{{route('upload.file')}}" accept-charset="UTF-8" id="uploadFormProfile" enctype="multipart/form-data">
  @csrf
  <input id="uploadFile" style="display:none;" accept=".doc,.docx" class="uploadFileProfile" name="file" type="file">
</form>

@stop
@section('scripts')
<script>
function cancel(){
    document.location = '{{route($route.'.index')}}';
}

$(document).on('ready',function() {
      $('.select2').select2();
      $('.singledatepicker').daterangepicker({
          singleDatePicker: true,
          showDropdowns: true,
          locale: {
            format: 'DD/MM/YYYY'
          }
      });
});

var elementNameUploadImageFor = null;
function uploadImageFor(elementName){
    elementNameUploadImageFor = elementName;
    $('#uploadImage').click();
}

var elementNameUploadFileFor = null;
function uploadFileFor(elementName){
    elementNameUploadFileFor = elementName;
    $('#uploadFile').click();
}

$(document).ready(function(){
    $('#uploadImage').change(function() {
        $( "#uploadImageForm" ).submit();
    });

    $('#uploadFile').change(function() {
        $( "#uploadFileForm" ).submit();
    });
});

$('#uploadImageForm').on('submit',(function(e) {
    e.preventDefault();
    var _token = $('input[name="_token"]').val();
    var formData = new FormData();
    formData.append('file', $('#uploadImage')[0].files[0]);
    formData.append('_token', _token)
    $.ajax({
        type:'POST',
        url: $(this).attr('action'),
        data:formData,
        cache:false,
        contentType: false,
        processData: false,
        beforeSend: function() {
            // $('.uploadProfile').html('<img src="' + window.location.origin + '/images/ajax-loading-64.gif" class="img-responsive" alt="">' + fa_camera_icon);
        },
        success:function(data) {
            $('[data-name='+elementNameUploadImageFor+']').attr('src','{{ url('/media/original/')}}/' + data.file_name);
            $('[name='+elementNameUploadImageFor+']').val(data.file_name);
        },
        error:function(data){
            
            
        }
    });
}));

$('#uploadFileForm').on('submit',(function(e) {
    e.preventDefault();
    var _token = $('input[name="_token"]').val();
    var formData = new FormData();
    formData.append('file', $('#uploadFile')[0].files[0]);
    formData.append('_token', _token)
    $.ajax({
        type:'POST',
        url: $(this).attr('action'),
        data:formData,
        cache:false,
        contentType: false,
        processData: false,
        beforeSend: function() {
            
        },
        success:function(data) {
            $('[name='+elementNameUploadFileFor+']').val(data.file_name);
        },
        error:function(data){
            
        }
    });
}));
</script>
@stop
