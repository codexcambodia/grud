<?php
namespace CodeClans\CRUD;

class RelatedCrudMultipleConfig{

    protected $virtualFieldName;
    protected $relatedCrudClassName;
    protected $selectedFields;
    protected $relationModelClassName;
	protected $relationFields;
    protected $defaultSavedValues;
    protected $whereQueries;

    public function __construct($virtualFieldName, $relatedCrudClassName, $selectedFields, $relationModelClassName, $relationFields, $defaultSavedValues, $whereQueries)
    {
        $this->virtualFieldName = $virtualFieldName;
        $this->relatedCrudClassName = $relatedCrudClassName;
        $this->selectedFields = $selectedFields;
        $this->relationModelClassName = $relationModelClassName;
        $this->defaultSavedValues = $defaultSavedValues;
        $this->whereQueries = $whereQueries;
		$this->relationFields = $relationFields;
    }

    public function getVirtualFieldName(){
		return $this->virtualFieldName;
	}

	public function setVirtualFieldName($virtualFieldName){
		$this->virtualFieldName = $virtualFieldName;
	}

	public function getRelatedCrudClassName(){
		return $this->relatedCrudClassName;
	}

	public function setRelatedCrudClassName($relatedCrudClassName){
		$this->relatedCrudClassName = $relatedCrudClassName;
	}

	public function getSelectedFields(){
		return $this->selectedFields;
	}

	public function setSelectedFields($selectedFields){
		$this->selectedFields = $selectedFields;
	}

	public function getRelationModelClassName(){
		return $this->relationModelClassName;
	}

	public function setRelationModelClassName($relationModelClassName){
		$this->relationModelClassName = $relationModelClassName;
	}

	public function getDefaultSavedValues(){
		return $this->defaultSavedValues;
	}

	public function setDefaultSavedValues($defaultSavedValues){
		$this->defaultSavedValues = $defaultSavedValues;
	}

	public function getWhereQueries(){
		return $this->whereQueries;
	}

	public function setWhereQueries($whereQueries){
		$this->whereQueries = $whereQueries;
	}

	public function getRelationFields(){
		return $this->relationFields;
	}

	public function setRelationFields($relationFields){
		$this->relationFields = $relationFields;
	}



}