<?php

namespace CodeClans\CRUD;

use Reliese\Support\Classify;
use Reliese\Coders\Model\Config;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\ServiceProvider;
use Reliese\Coders\Console\CodeModelsCommand;
use Reliese\Coders\Model\Factory as ModelFactory;

class CrudServiceProvider extends ServiceProvider
{
    /**
     * @var bool
     */
    protected $defer = true;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__ . '/views' => base_path('resources/views/'),
                __DIR__.'/../config/crud.php' => config_path('crud.php'),
            ], 'codeclans-crud');

        }
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        
    }

    

    /**
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
