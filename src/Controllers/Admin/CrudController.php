<?php
namespace CodeClans\CRUD\Controllers\Admin;
use App\Models\TranslatableModel;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Routing\Controller as BaseController;
use CodeClans\CRUD\RelatedCrudMultipleConfig;

class CrudController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $model = TranslatableModel::class;
    protected $route = '';

    protected $listFields = [];
    protected $formFields = [];

    protected $fieldCreatedBy = '';
    protected $fieldModifiedBy = '';

    /*
    |--------------------------------------------------------------------------
    | List of model field that will display image upload
    |--------------------------------------------------------------------------
    | Format:
    | [
    |    'field' => [
    |          'list_table_name',
    |          'primary_field',
    |          'display_field'
    |     ],
    | ]
    */
    protected $singleSelects = [];
    

    /*
    |--------------------------------------------------------------------------
    | List of model field that will display image upload
    |--------------------------------------------------------------------------
    | Format : 
    | [
    |    'field' => [
    |          width,
    |          height
    |    ],
    | ]
    */
    protected $imageFields = [];

    protected $fileFields = [];

    /*
    |--------------------------------------------------------------------------
    | List of model field that will diplay selection base on dict config
    |--------------------------------------------------------------------------
    | please go to config/curd.php to update the dictionary
    | Format : 
    | [
    |    'field' => 'dictionary_name',
    | ]
    */
    protected $dictionaryFields = [];
    

    protected $textAreaFields = [];

    protected $dateFields = [];

    protected $item = null;
    protected $id = null;

    protected $extendedFormViews = [];

    protected $passwordFields = [];
    protected $passwordConfirmationFields = [];

    protected $rawListConditions = [];


    public function __construct()
    {
        $this->transformDictField();   
        $this->createRelatedCrudMultiple();
        
    }



    protected $transformedDictFields = [];
    protected $releatedCrudMultipleFields = [];
    protected $releatedCrudMultipleViews = [];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = $this->list();
        $table = $this->table();
        $dateFields = $this->dateFields;
        $convertedSingleSelectSources = $this->convertedSingleSelectSources();
        $data = [
            'list' => $list,
            'table' => $table,
            'route' => $this->route,
            'dateFields' => $dateFields,
            'convertedSingleSelectSources' => $convertedSingleSelectSources,
            'extended' => $this->extendedListData(),
            'imageFields' => $this->imageFields,
            'transformedDictFields' => $this->transformedDictFields
        ];
        return view('admin.crud.index', $data);
    }

    public function convertedSingleSelectSources()
    {
        // $sources = $this->getSingleSelectSources();
        // $singleSelectSources = $this->getSingleSelectSources();
        // foreach($singleSelectSources as $singleSelectSourceKey => $singleSelectSource){

        //     $data = [];
        //     foreach($singleSelectSource as $record){
        //         $data[$record->id] = $record->display;
        //     }
        //     $sources[$singleSelectSourceKey] = $data;
        // }

        // return $sources;

        return $this->getSingleSelectSources();
    }

    public function indexData(Request $request)
    {
        Log::debug($request);
        // dd($request);
        $model = new $this->model();
        // dd($model->select('name','name_alt','id')->toSql());
        $records = $model->select('*');
        foreach($this->rawListConditions as $condition){
            $records->whereRaw($condition);
        }
        return DataTables::of($records)
            ->addIndexColumn()
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $form = $this->form();
        $table = $this->table();
        $singleSelectSources = $this->getSingleSelectSources();
        $textAreaFields = $this->textAreaFields;
        $dateFields = $this->dateFields;
        $extendedFormViews = $this->extendedFormViews;
        $passwordFields = $this->passwordFields;
        $passwordConfirmationFields = $this->passwordConfirmationFields;
        $this->generateRelatedCrudMultipleViews();
        $data = [
            'form' => $form,
            'table' => $table,
            'route' => $this->route,
            'singleSelectSources' => $singleSelectSources,
            'textAreaFields' => $textAreaFields,
            'dateFields' => $dateFields,
            'extended' => $this->extendedCreateData(),
            'extendedFormViews' => $extendedFormViews,
            'imageFields' => $this->imageFields,
            'fileFields' => $this->fileFields,
            'requiredFields' => [], //$this->getRequiredFields(),
            'passwordFields' => $passwordFields,
            'passwordConfirmationFields' => $passwordConfirmationFields,
            'dictionaryFields' => $this->dictionaryFields,
            'transformedDictFields' => $this->transformedDictFields,
            'releatedCrudMultipleViews' => $this->releatedCrudMultipleViews
        ];
        if ($form->number_of_column === 1) {
            return view('admin.crud.form', $data);
        }
        return view('admin.crud.form-two-column', $data);
    }

    public function form()
    {
        $model = new $this->model();
        $fields = $model->getFillable();
        if (count($this->formFields) !== 0) {
            $fields = $this->formFields;
        }
        $formFields = [];
        foreach ($fields as $field) {
            $formFields[] = (object) [
                'field' => $field,
                'type' => 'textbox',
            ];
        }

        $form = null;

        //build form with two column or not
        $limitFieldsPerColumn = 3;
        $totalFields = count($formFields);
        $numberOfColumns = 1;
        if ($totalFields > $limitFieldsPerColumn) {
            $numberOfColumns = 2;
        }

        if ($numberOfColumns === 1) {
            $form = (object) [
                'number_of_column' => 1,
                'rows' => [
                    $formFields,
                ],
            ];
        } elseif ($numberOfColumns === 2) {
            $form = (object) [
                'number_of_column' => 2,
                'rows' => [],
            ];
            $fieldIteration = 0;
            while ($fieldIteration < $totalFields) {
                $rowIndex = (int) ($fieldIteration / 2);

                if (! array_key_exists($rowIndex, $form->rows)) {
                    $form->rows[$rowIndex] = [];
                }

                $form->rows[$rowIndex][] = $formFields[$fieldIteration];
                $fieldIteration++;
            }
        }

        return $form;
    }

    public function table()
    {
        $model = new $this->model();
        return $model->getTable();
    }

    public function getSingleSelectSources()
    {
        $sources = [];
        foreach ($this->singleSelects as $singleSelectKey => $singleSelect) {
            $table = $singleSelect[0];
            $idField = $singleSelect[1];
            $displayField = $singleSelect[2];
            // $sources[$singleSelectKey] = DB::table($table)->select($idField.' as id',$displayField.' as display')->get();
            $source = DB::table($table)->whereNull('deleted_at');
            if(count($singleSelect)==4){
                $source->whereRaw($singleSelect[3]);
            }
            $sources[$singleSelectKey] = $source->select($idField . ' as id', $displayField . ' as display')->pluck('display', 'id')->toArray();
            $sources[$singleSelectKey] = [ '' => __('Choose one')] + $sources[$singleSelectKey];
        }
        return $sources;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requiredFields = $this->getRequiredFields();
        if (count($requiredFields) > 0) {
            $rules = [];
            foreach ($requiredFields as $requiredField) {
                if(!in_array($requiredField,$this->passwordFields)){
                    $rules[$requiredField] = 'required';
                }
                
            }
            $request->validate($rules);
        }

        $model = new $this->model();
        $moreData = [];
        if ($this->fieldCreatedBy !== '') {
            $moreData[$this->fieldCreatedBy] = auth()->id();
        }
        $inputAll = $request->all();
        foreach ($this->dateFields as $dateField) {
            // ignore fields not in form
            if (in_array($dateField, $this->formFields)) {
                $input = $inputAll[$dateField];
                $input = date_create_from_format('d/m/Y', $input);
                $input = date_format($input, 'Y-m-d');
                $inputAll[$dateField] = $input;
            }
        }
        // convert password field
        foreach ($this->passwordFields as $passwordField) {
            // ignore fields not in form
            if (in_array($passwordField, $this->formFields)) {
                $input = $inputAll[$passwordField];
                $input = ($input);
                $inputAll[$passwordField] = $input;
            }
        }
        $model->fill($inputAll + $moreData);
        $model->save();
        $this->item = $model;
        $this->extendedStoreData();
        $this->saveRelatedCrudMultiple();
        return redirect(route($this->route . '.index'))->with('successful', __('Save successful!'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        Log::debug($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->id = $id;
        $form = $this->form();
        $table = $this->table();
        $model = new $this->model();
        $this->item = $model->find($id);
        $singleSelectSources = $this->getSingleSelectSources();
        $textAreaFields = $this->textAreaFields;
        $dateFields = $this->dateFields;
        $extendedFormViews = $this->extendedFormViews;
        $passwordFields = $this->passwordFields;
        $passwordConfirmationFields = $this->passwordConfirmationFields;
        $this->generateRelatedCrudMultipleViews();
        $data = [
            'form' => $form,
            'table' => $table,
            'route' => $this->route,
            'item' => $this->item,
            'singleSelectSources' => $singleSelectSources,
            'textAreaFields' => $textAreaFields,
            'dateFields' => $dateFields,
            'extended' => $this->extendedEditData(),
            'extendedFormViews' => $extendedFormViews,
            'imageFields' => $this->imageFields,
            'fileFields' => $this->fileFields,
            'requiredFields' => $this->getRequiredFields(),
            'passwordFields' => $passwordFields,
            'passwordConfirmationFields' => $passwordConfirmationFields,
            'dictionaryFields' => $this->dictionaryFields,
            'transformedDictFields' => $this->transformedDictFields,
            'releatedCrudMultipleViews' => $this->releatedCrudMultipleViews
        ];
        if ($form->number_of_column === 1) {
            return view('admin.crud.form', $data);
        }
        return view('admin.crud.form-two-column', $data);
    }

    public function extendedListData()
    {
        return [];
    }

    public function extendedCreateData()
    {
        return [];
    }

    public function extendedEditData()
    {
        return [];
    }

    public function extendedStoreData()
    {
    }

    public function extendedUpdateData()
    {
    }

    public function list()
    {
        $list = [];
        $model = new $this->model();
        $fields = $model->getFillable();
        if (count($this->listFields) !== 0) {
            $fields = $this->listFields;
        }
        foreach ($fields as $field) {
            $list[] = (object) [
                'field' => $field,
                'type' => 'textbox',
            ];
        }
        return $list;
    }

    public function listForRelative($selectedFields)
    {
        $list = [];
        $model = new $this->model();
        $fields = $model->getFillable();
        if (count($selectedFields) !== 0) {
            $fields = $selectedFields;
        }
        foreach ($fields as $field) {
            $list[] = (object) [
                'field' => $field,
                'type' => 'textbox',
            ];
        }
        return $list;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $requiredFields = $this->getRequiredFields();
        if (count($requiredFields) > 0) {
            $rules = [];
            foreach ($requiredFields as $requiredField) {
                if(!in_array($requiredField,$this->passwordFields)){
                    $rules[$requiredField] = 'required';
                }
                
            }
            $request->validate($rules);
        }

        $model = new $this->model();
        $record = $model->find($id);
        $moreData = [];
        if ($this->fieldModifiedBy !== '') {
            $moreData += [$this->fieldModifiedBy => auth()->id()];
        }
        $inputAll = $request->all();
        foreach ($this->dateFields as $dateField) {
            // ignore fields not in form
            if (in_array($dateField, $this->formFields)) {
                $input = $inputAll[$dateField];
                $input = date_create_from_format('d/m/Y', $input);
                $input = date_format($input, 'Y-m-d');
                $inputAll[$dateField] = $input;
            }
        }
        // convert password field
        foreach ($this->passwordFields as $passwordField) {
            // ignore fields not in form
            if (in_array($passwordField, $this->formFields)) {
                $input = $inputAll[$passwordField];
                if ($input !== '' && $input !== null) {
                    $input = ($input);
                    $inputAll[$passwordField] = $input;
                } else {
                    unset($inputAll[$passwordField]);
                }
            }
        }

        $record->update($inputAll + $moreData);

        $this->item = $record;
        $this->extendedUpdateData();
        $this->saveRelatedCrudMultiple();
        return redirect(route($this->route . '.index'))->with('successful', __('Save successful!'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        Log::debug($request);
        $model = new $this->model();
        $record = $model->find($id);
        $record->delete();
        return redirect(route($this->route . '.index'))->with('successful', __('Delete successful!'));
    }

    public function getRequiredFields()
    {
        $fields = [];
        $model = new $this->model();
        $table = $model->getTable();
        $originalColumns = Schema::getConnection()->getDoctrineSchemaManager()->listTableColumns($table);
        foreach ($originalColumns as $column) {
            if ($column->getNotnull()) {
                $columnName = $column->getName();
                if (! in_array($columnName, [$this->fieldCreatedBy, $this->fieldModifiedBy, 'id'])) {
                    $fields[] = $columnName;
                }
            }
        }

        return $fields;
    }

    private function transformDictField(){
    
        $fields = [];
        foreach($this->dictionaryFields as $dictField => $dictName){
            $selectSource = [];
            $selectSource[''] = ' ';
            foreach(config('crud.dict.'.$dictName, []) as $value){
                $selectSource[$value] = __('crud.dict.'.$dictName.'.'.$value);
            }
            $fields[$dictField] = $selectSource;
        }
        $this->transformedDictFields = $fields;
    }


    public function getViewsForRelativeCrud(String $virtualFieldName,array $selectedFields, array $selectedIDs ){
        $list = $this->listForRelative($selectedFields);
        $table = $this->table();
        $dateFields = $this->dateFields;
        $convertedSingleSelectSources = $this->convertedSingleSelectSources();
        $data = [
            'list' => $list,
            'table' => $table,
            'route' => $this->route,
            'dateFields' => $dateFields,
            'convertedSingleSelectSources' => $convertedSingleSelectSources,
            'extended' => $this->extendedListData(),
            'imageFields' => $this->imageFields,
            'transformedDictFields' => $this->transformedDictFields,
            'virtualFieldName' => $virtualFieldName,
            'selectedIDs' => $selectedIDs
        ];
        
        $html = view('admin.crud.relative', $data)->render();
        return $html;
    }

    protected function createRelatedCrudMultiple(){
        //sample
        
    }

    protected function addRelatedCrudMultiple(String $virtualFieldName,String $relatedCrudClassName,array $selectedFields, String $relationModelClassName, array $relationFields, array $defaultSavedValues,array $whereQueries){
      
        $this->releatedCrudMultipleFields[$virtualFieldName] = new RelatedCrudMultipleConfig($virtualFieldName, $relatedCrudClassName, $selectedFields, $relationModelClassName, $relationFields, $defaultSavedValues, $whereQueries);
        
    }

    protected function generateRelatedCrudMultipleViews(){
        $this->releatedCrudMultipleViews = [];
        foreach($this->releatedCrudMultipleFields as $releatedCrudMultipleField){
            $virtualFieldName = $releatedCrudMultipleField->getVirtualFieldName();
            $relationModelClassName = $releatedCrudMultipleField->getRelationModelClassName();
            $relationFields = $releatedCrudMultipleField->getRelationFields();
            $defaultSavedValues = $releatedCrudMultipleField->getDefaultSavedValues();
            $relatedCrudClassName = $releatedCrudMultipleField->getRelatedCrudClassName();
            $whereQueries = $releatedCrudMultipleField->getWhereQueries();
            $selectedFields = $releatedCrudMultipleField->getSelectedFields();

            $relatedCrudController = new $relatedCrudClassName();
            $value = $this->getValueOfRelatedCrudMultiple($relationModelClassName, $relationFields,$whereQueries);
            $viewHtml = $relatedCrudController->getViewsForRelativeCrud($virtualFieldName, $selectedFields, $value );
            $this->releatedCrudMultipleViews[$virtualFieldName] = $viewHtml;
    
        }
    }



    private function getValueOfRelatedCrudMultiple($relationModelClassName, $relationFields,$whereQueries){
        
        $value = [];
        
        if($this->item){
            $id = $this->item->id;
            
            $model = new $relationModelClassName();
            $value = $model->where($relationFields[0],$id)->pluck($relationFields[1])->toArray();

            
        }
        
        
        return $value;
    }

    private function saveRelatedCrudMultiple(){
        
        foreach($this->releatedCrudMultipleFields as $releatedCrudMultipleField){
            /* @var $releatedCrudMultipleField RelatedCrudMultipleConfig */
            $virtualFieldName = $releatedCrudMultipleField->getVirtualFieldName();
            $dataString = request()->get($virtualFieldName,'');
            $data = explode(',',$dataString);
            $relationModelClassName = $releatedCrudMultipleField->getRelationModelClassName();
            $relationFields = $releatedCrudMultipleField->getRelationFields();
            $defaultSavedValues = $releatedCrudMultipleField->getDefaultSavedValues();
            //delete old relation
            $model = new $relationModelClassName();
            $query = $model->where($relationFields[0],$this->item->id)->delete();
            foreach($data as $id){
                $model = new $relationModelClassName();
                $fieldData = [
                    $relationFields[0] => $this->item->id,
                    $relationFields[1] => $id,
                ];
                $fieldData += $defaultSavedValues;
                $model->fill($fieldData);
                $model->save();
            }
            
        }

    }
}
